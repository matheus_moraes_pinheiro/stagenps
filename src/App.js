import React from "react";
import GlobalStyle from './styles/global'
import Form from './component/Form'
import Grid from '@material-ui/core/Grid';


function App() {
  let form = {
    item: true,
    xs: 12,
    sm: 12
  }

  let GridStyle = {
    backgroundColor: 'white',
    borderRadius: 10,
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'space-between',
    alignItems: 'center',
    width: '100%',
    overflow: 'hidden'
  }

  return (
    <>

      <Grid 
        container 
        alignItems='center' >
          
        <Grid {...form}
          style={GridStyle}>
          <Form />
        </Grid>
      </Grid>
      <GlobalStyle />
    </>
  );
}

export default App;
