import styled from 'styled-components';

export const Global = styled.div`
    display: flex;
    flex-direction: row;
`;
export const Nav = styled.div`
    width: 100%;
    height: 56px;
    z-index: 2;
    background-color: #F7F2FE;
    display: flex;
    align-items: center;
    justify-content: center;
`;
export const Rating = styled.div`
    margin: 32px 0;
    display: grid;
    grid-template-columns: repeat(5, 1fr);
    grid-column-gap: 24px;
    grid-row-gap: 24px;
`;


export const ProtocolosImg = styled.img`
  width: 100px;
  height: 100px;
`;
export const RatingButton = styled.div`
    width: 40px;
    height: 40px; 
    background-color: #F0F0F0; 
    border-radius: 4px;
    display: flex;
    justify-content: center;
    align-items: center;
    @media(min-width: 800px) {
        /* margin:4px; */
    }
`;
export const Home = styled.div`
    width: 100%;
    height: 800px;
    transition: transform 1s;
    text-align: center;
    display: flex;
    flex-direction: column;
    justify-content: space-around;
    margin: auto;
    .banner-home {
        width: 350px;
    }
    @media(min-width: 800px) {
        flex-direction: row;
        margin-bottom: 0;
        align-items: center;
        justify-content: center;
        .banner-home {
            width: 600px;
        }
  }
`;
export const BgBlue = styled.div `
    width: 100%;
    background-color: #6200EE;
    margin-bottom: 0;
    @media(max-width: 800px) {
        display: none;
    }
`;
export const Questions = styled.div `
    width: 100%;
    height: 100vh;
    display: grid;
    grid-template-columns: repeat(2, 1fr);
    grid-auto-rows: minmax(100px, auto);
    @media(max-width: 800px) {
        grid-template-columns: repeat(1, 1fr);
    }
`;
export const SucessContainer = styled.div`
    width: 100%;
    height: 800px;
    transition: transform 1s;
    text-align: center;
    display: flex;
    flex-direction: column;
    justify-content: space-around;
    margin: auto;
    .banner-home {
        width: 350px;
    };

    @media(min-width: 800px) {
        flex-direction: row;
        margin-bottom: 0;
        align-items: center;
        justify-content: center;
        .banner-home {
            width: 600px;
        };
    }
`;
export const ButtonNavigation = styled.div`
    display: flex; 
    flex-direction: row; 
    justify-content: space-between; 
    width: 100%;
    padding: 10px;
    @media(min-width: 800px) {
        max-width: 400px;
        margin: 0 auto;
        padding-bottom: 10px;
    }
    @media(max-width: 800px) {
        width: 100%;
        padding: 10px;
    }
    h2 {
        font-size:14px;
    }
`
export const ButtonPrevious = styled.div`
    margin-bottom: 0rem;
    display: flex; 
    flex-direction: row; 
    align-items: center; 
    margin: 10;
    justify-content: flex-start;
`
export const ButtonNext = styled.div`
    margin-bottom: 0rem;
    display: flex; 
    flex-direction: row; 
    align-items: center; 
    justify-content: flex-start;
`
export const MountCard = styled.div`
    margin: 20px;
    display: flex;
    height: 600px;
    align-items: center;
    @media(min-width: 800px) {
        width: 400px;
        margin: auto;
    }
`
export const ProgressBar = styled.div`
    color: #898989;
    margin-left: 10px;
    margin-right: 10px;
    margin-top: 24px;
    @media(min-width: 800px) {
        width: 400px;
        margin: 24px auto;
    }
`