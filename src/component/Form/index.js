import React, { useState } from 'react';
import { useParams } from 'react-router';

import 'animate.css';
import "react-toastify/dist/ReactToastify.css";
import { ToastContainer, toast, cssTransition } from "react-toastify";
import { 
  Nav, 
  Rating, 
  RatingButton, 
  Home, 
  BgBlue, 
  Questions, 
  SucessContainer, 
  ButtonNavigation, 
  ButtonPrevious, 
  ButtonNext,
  MountCard,
  ProgressBar 
} from './styles';

import Logo from "../../assets/images/logo-stg.svg";
import BannerHome from "../../assets/images/stg-illustracao-home.svg"
import BannerSucess from "../../assets/images/stg-illustracao-sucess.svg"
import Button from '../Button'

import TextField from '@material-ui/core/TextField';
import Grid from '@material-ui/core/Grid';
import LinearProgress from '@material-ui/core/LinearProgress';
import ExpandMore from '@material-ui/icons/ExpandMore';
import ExpandLess from '@material-ui/icons/ExpandLess';
import { makeStyles } from '@material-ui/core/styles';
import Backdrop from '@material-ui/core/Backdrop';
import CircularProgress from '@material-ui/core/CircularProgress';
import { Domain } from '@material-ui/icons';

import Radio from '@mui/material/Radio';
import RadioGroup from '@mui/material/RadioGroup';
import FormControlLabel from '@mui/material/FormControlLabel';
import FormControl from '@mui/material/FormControl';
import FormLabel from '@mui/material/FormLabel';


const useStyles = makeStyles((theme) => ({
  root: {
    width: '100%',
  },  
  backdrop: {
    zIndex: theme.zIndex.drawer + 1,
    color: '#fff',
  },
}));
let errs = {
    required: 'Campo obrigatório'
}
const bounce = cssTransition({
  enter: "animate__animated animate__bounceIn",
  exit: "animate__animated animate__bounceOut"
});

function Form() {
  const { idUser } = useParams()

  const classes = useStyles();

  const [form, setForm] = useState(0)
  const [data, setData] = useState({
    id: idUser,
    recomenda: '',
    motivo: '',
    recontratar: '',
    depoimento: '',
    data: new Date(),
    publicar: null
  })
  const [page, setPage] = useState('home')
  const [open, setOpen] = useState(false);
  const [idColorRecomendar, setIdColorRecomendar] = useState()
  const [idColorRecontratar, setIdColorRecontratar] = useState()
  const [buttomNext, setButtomNext] = useState(false);
  
  const handleClose = () => {
    setOpen(false);
  };
  const handleSubmit = async (e) => {
    setOpen(!open)
    e.preventDefault()
    try {
      const res = await fetch("https://sheet.best/api/sheets/c821b300-4668-4e71-b6ff-e3138b8f95d8", {
        method: 'POST',
        headers: { "Content-Type": "application/json"},
        body: JSON.stringify(data),
      })
      if(res.ok) {
        setTimeout(() => setPage('sucess'), 400)
      }
    } catch (error) {
      console.log(error)
    }
  }

  const rating = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
  
  const ComponentsData = [
    { id: 1, name: 'recomendar', title: '1. Qual a probabilidade de você recomendar a Stage Consulting para um amigo, colega, ou outra empresa?', value: data.recomenda, node: 
      <div>
        <Rating xs={12}>
          {rating.map(a => 
            <RatingButton name='recomenda' id={"recomenda"+a} 
              style={idColorRecomendar === a ? 
                {backgroundColor: '#6200EE', color: '#fff'} : 
                {backgroundColor: '#F0F0F0', color: '#000'} }
              onClick={() => {
                setButtomNext(true)
                setData({...data, recomenda: a})
                setIdColorRecomendar(a)
              }}
            >
              <p style={{ fontWeight: 'bold' }}>{a}</p>
            </RatingButton>
          )}
        </Rating>
            <div style={{display: 'flex', justifyContent: 'space-between', color: '#898989'}}>
              <div>1- Pouco provável</div>
              <div>10 - Muito provável</div>
            </div>
        
          <div style={{ position: 'fixed', marginTop: 20, width: '100%', display: 'flex', justifyContent: 'flex-end'  }}>
            <div style={{display: 'flex'}}>
              { data.recomenda ? 
                <Button onClick={() => next()}>OK!</Button> : <div></div>}
            </div> 
          </div>
       
      </div>
    },
    { id: 2, name: 'motivo', title: '2. Qual o principal motivo por ter dado essa nota?', value: data.motivo, node:
      <TextField
        style={{ marginTop: 32 }}
        type='text'
        fullWidth
        multiline
        rows={4}
        helperText={errs.required}
        variant="filled"
        label="Digite aqui sua resposta"
        name='motivo'
        value={data.motivo}
        onChange={e => {
          setData({...data, motivo: e.target.value})
          setButtomNext(true)
        }}
      /> 
    },
    { id: 3, name: 'recontratar', title: '3. Qual a probabilidade de você recontratar um novo serviço da Stage Consulting?', value: data.recontratar, node:
      <div>
      <Rating xs={12}>
        {rating.map(a => 
          <RatingButton name='recontratar' id={"recontratar"+a}
          style={idColorRecontratar === a ? 
            {backgroundColor: '#6200EE', color: '#fff'} : 
            {backgroundColor: '#F0F0F0', color: '#000'} }
            onClick={() => {
              setButtomNext(true)
              setData({...data, recontratar: a})
              setIdColorRecontratar(a)
            }}
          >
            <p style={{ fontWeight: 'bold' }}>{a}</p>
          </RatingButton>
        )}
      </Rating>
          <div style={{display: 'flex', justifyContent: 'space-between', color: '#898989'}}>
            <div>1- Pouco provável</div>
            <div>10 - Muito provável</div>
          </div>
      </div>
    },
    { id: 4, name: 'motivo', title: '4. Deixe o seu depoimento sobre como foi trabalhar com a Stage', value: data.depoimento, node:
      <TextField
        style={{ marginTop: 32 }}
        type='text'
        fullWidth
        multiline
        rows={4}
        helperText={errs.required}
        variant="filled"
        label="Digite aqui sua resposta"
        name='motivo'
        value={data.depoimento}
        onChange={e => {
          setData({...data, depoimento: e.target.value})
          setButtomNext(true)
        }}
      /> 
    },

    { id: 5, name: 'publicar', title: '5. Obrigado pelo seu depoimento! Podemos publicá-lo em nossas redes sociais e/ou no nosso site?', value: data.publicar, node:
      <div>
        <FormControl>
          <RadioGroup
            row
            aria-labelledby="demo-radio-buttons-group-label"
            defaultValue="female"
            name="radio-buttons-group"
          >
            <FormControlLabel value="1" control={<Radio />} label="Sim" 
              onClick={() => {
                setData({...data, publicar: 1})
              }}/>
            <FormControlLabel value="0" control={<Radio />} label="Não"
              onClick={() => {
                setData({...data, publicar: 0})
              }}/>
                  
          </RadioGroup>
        </FormControl>
        <div style={{ position: 'fixed', width: '100%', display: 'flex', justifyContent: 'flex-end'  }}>
          <div style={{display: 'flex'}}>
          { data.publicar != null ? 
              <Button onClick={handleSubmit}>FINALIZAR</Button> : <div></div>}
          </div>
        </div>
      </div>
    },
  ]

  const animated = () => {
    const component = ComponentsData.filter(i => i.id === form+1)[0]
    component.value ? setButtomNext(true) :  setButtomNext(false)
    document.getElementById(form).classList.add('animate__fadeOutUp')
      if (form === ComponentsData.length) {
        setTimeout(() => setPage('sucess'), 400)
      } else {
        const nextCard = form +1
        setTimeout(() => {
          document.getElementById(form).classList.remove('animate__fadeOutUp')
          setForm(nextCard)
        }, 400)
      }
  }
  const next = () => {
    const component = ComponentsData.filter(i => i.id === form)[0]
    form === 0 ? 
      animated() : 
      component.value ?
        animated() : 
        emptyField()
  }
  const previous = () => {
    setButtomNext(true)
    document.getElementById(form).classList.add('animate__fadeOutUp')
    const previousCard = form -1
    form !== 1 ?
      setTimeout(() => {
        document.getElementById(form).classList.remove('animate__fadeOutUp')
        setForm(previousCard)
      }, 400) :
      setTimeout(() => setPage('home'), 400)
  }
  const mount = () => ComponentsData.filter(i => i.id === form).map(a => 
    <MountCard 
      className="animate__animated animate__fadeInUp" 
      id={a.id} >

      <Grid xs={12}>
        <div>
          <h2 style={{ marginBottom: 10}}><strong>{a.title}</strong></h2>
          <div children={a.node}></div>          
        </div>
      </Grid>
    </MountCard> 
)
  const emptyField = () => {
    toast.warn('Preencha todos os campos!', {
      position: "top-right",
      autoClose: 3000,
      hideProgressBar: false,
      closeOnClick: true,
      pauseOnHover: true,
      draggable: true,
      progress: undefined,
      });
  }
  function home() {
    return (
      <> 
              <Home className="animate__animated animate__fadeInUp" id="0" >
                  <div>
                    <img className="banner-home" src={BannerHome} alt='banner-home' />
                  </div>
                  <div>
                    <div>
                      <img alt='logo' src={Logo} />
                    </div>
                    <div>
                      <p style={{color: '#6200EE', fontSize: 12, marginBottom: 10}}>(5 MIN)</p>
                      <h1>Sua opinião é muito<br />
                      importante para nós</h1>
                      <p style={{ marginTop: 10, fontSize: 18 }}>Participe da nossa pesquisa de satisfação. <br />São só 5 perguntas.</p>
                    </div>

                    <div style={{ margin: '1rem' }}>
                            {
                              <Button style={{ marginBottom: 40 }}
                                width={100}
                                onClick={() => {
                                  setPage('form')
                                  setForm(1)}}
                                > INICIAR
                              </Button>
                            }
                    </div>
                  </div>
              </Home> 


      </>
    )
  }

  function questions() {
    return (

      <Questions>
        <Backdrop className={classes.backdrop} open={open} onClick={handleClose}>
          <CircularProgress color="inherit" />
        </Backdrop>
        <BgBlue></BgBlue>
        <div style={{ 
          display: 'flex', flexDirection: 'column', justifyContent: 'space-between'
           }}>

          {/* header logo */}
          <div>

            <Nav>
              <img alt='logo' src={Logo} />
            

            </Nav>
            {/* linha de progresso */}
            <ProgressBar>{form}/5
              <div 
              className={classes.root}
              >
                <LinearProgress 
                  variant="determinate" 
                  color="primary" 
                  value={form*(100/5)} />
              </div>
              <ToastContainer transition={bounce} style={{ marginTop: '80px'}} />
            </ProgressBar>
          </div>

          {/* questoes */}
          {mount()}

          {/* botoes de navegação */}
          <ButtonNavigation>
                {form !== 1 ? 
                    <ButtonPrevious>
                          <div>
                              <ExpandLess style={{ 
                                width: 40, 
                                height: 40, 
                                color: '#6200EE', 
                                textAlign: 'center',
                                backgroundColor: '#F7F2FE',
                                borderRadius: 4
                              }} onClick={() => previous()} />
                                
                          </div>
                          <h2 style={{ margin: 10 }}>Anterior</h2>
                        </ButtonPrevious> 
                : <div></div>}
                {form !== ComponentsData.length ? 
                  <ButtonNext>
                    <h2 style={{ margin: 10, color: buttomNext ? '#000' : '#C4C4C4' }}>Próximo</h2>
                    <div>
                        <ExpandMore style={{ 
                          width: 40, 
                          height: 40, 
                          color: buttomNext ? '#6200EE' : '#fff', 
                          textAlign: 'center',
                          backgroundColor: buttomNext ? '#F7F2FE' : '#F0F0F0',
                          borderRadius: 4
                        }} onClick={() => buttomNext?  next() : ''} />
                    </div>
                  </ButtonNext> 
                : <div></div>}
          </ButtonNavigation>
    
        </div>
      </Questions>
    )
  }
  function sucess () {
    return (
      <>

        <SucessContainer className="animate__animated animate__fadeInUp" >
          <div>
            <img className="banner-home" src={BannerSucess} alt='banner-home' />
          </div>
          <div>
            <div>
              <img alt='logo' src={Logo} />
            </div>

            <div>
              <h1>Obrigado por participar!</h1>
              <p style={{ marginTop: 10, fontSize: 18 }}>Sua opinião é muito <br />importante para nós.</p>
            </div>

            <div style={{ margin: '1rem' }}>
              {
                // <Button style={{ marginBottom: 40 }}
                //   width={100}
                //   onClick={() => {}}
                //   > VISITAR SITE
                // </Button>
              }
            </div>
          </div>
        </SucessContainer> 

      </>
    )
  }

    return (
      <>
      {page !== 'form' ? <Nav>
        <img alt='logo' src={Logo} />
      </Nav>: ('')}
      
      {page === 'home' ? home() :
        page === 'form' ? questions() : sucess()}
    </>
    );
  }
  
  export default Form;
  