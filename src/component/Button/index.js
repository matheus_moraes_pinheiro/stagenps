import React from 'react'
import { Filled } from './styles'

const Button = (props) => {

    const { Disabled, Type, Outlined, Naked, ...rest } = props


        return (
            <Filled bg={
                props.Disabled ? 'whitesmoke' : '#6200EE'
            }>
                <button {...rest} disabled={Disabled} type={Type} >
                    {props.children}
                </button>
            </Filled>
        )
    
}

export default Button