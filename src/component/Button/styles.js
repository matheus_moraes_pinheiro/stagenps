import styled from 'styled-components';


export const Filled = styled.div` 

    button{ 
        font-size: 1em;
        font-weight: 800;
        line-height: 1.75;
        text-transform:none;
        text-decoration:none; 
        padding: 5px 15px;
        color:white;

        transition: background 0.5s ease-in-out;
        background: ${props => props.bg};
        background-size: 150% auto;
        background-position: 0%;

        margin: 10px 0px 10px 0px;
        min-width: 100%;
        min-height:45px;
        justify-content: center;
    
        border-radius: 8px;
        border: none;
    
        cursor: pointer;
    }

    button:hover{ 
        background-position: 100%;
        background: #6200EE;
        color:white;
    }

    button.hamburger:hover{
        background:red!important;
    }

    button:disabled{
        cursor: default !important;
        background-color: #DFDFDF;
    }
    button:hover:disabled{
        cursor: default !important;
        background-color: whitesmoke !important;
    }
`;